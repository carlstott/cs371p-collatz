.DEFAULT_GOAL := all
SHELL         := bash

ifeq ($(shell uname -s), Darwin)
    ASTYLE        := astyle
    BOOST         := /usr/local/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := clang++
    CXXFLAGS      := --coverage -g -std=c++20 -I$(INCLUDE_PATH) -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := llvm-cov gcov
    GTEST         := /usr/local/include/gtest
    LDFLAGS       := -lgtest -lgtest_main
    LIB           := $(LIBRARY_PATH)
    VALGRIND      :=
else ifeq ($(shell uname -p), x86_64)
    ASTYLE        := astyle
    BOOST         := /lusr/opt/boost-1.82/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++-11
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov-11
    GTEST         := /usr/local/include/gtest
    LDFLAGS       := -L/usr/local/opt/boost-1.77/lib/ -lgtest -lgtest_main -pthread
    LIB           := /usr/local/lib
    VALGRIND      := valgrind-3.17
else
    ASTYLE        := astyle
    BOOST         := /usr/include/boost
    CHECKTESTDATA := checktestdata
    CPPCHECK      := cppcheck
    CXX           := g++
    CXXFLAGS      := --coverage -g -std=c++20 -Wall -Wextra -Wpedantic
    DOXYGEN       := doxygen
    GCOV          := gcov
    GTEST         := /usr/include/gtest
    LDFLAGS       := -lgtest -lgtest_main -pthread
    LIB           := /usr/lib
    VALGRIND      := valgrind
endif

# run docker
docker:
	docker run --rm -i -t -v $(PWD):/usr/gcc -w /usr/gcc gpdowning/gcc

# get git config
config:
	git config -l

# get git log
Collatz.log.txt:
	git log > Collatz.log.txt

# get git status
status:
	make clean
	@echo
	git branch
	git remote -v
	git status

# download files from the Collatz code repo
pull:
	make clean
	@echo
	git pull
	git status

# upload files to the Collatz code repo
push:
	make clean
	@echo
	git add .gitignore
	git add .gitlab-ci.yml
	git add Collatz.cpp
	git add Collatz.hpp
	-git add Collatz.log.txt
	-git add html
	git add Makefile
	git add README.md
	git add run_Collatz.cpp
	git add run_Collatz.ctd.txt
	git add test_Collatz.cpp
	git commit -m "another commit"
	git push
	git status

# compile run harness
run_Collatz: Collatz.hpp Collatz.cpp run_Collatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) run_Collatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp run_Collatz.cpp -o run_Collatz

# compile test harness
test_Collatz: Collatz.hpp Collatz.cpp test_Collatz.cpp
	-$(CPPCHECK) Collatz.cpp
	-$(CPPCHECK) test_Collatz.cpp
	$(CXX) $(CXXFLAGS) Collatz.cpp test_Collatz.cpp -o test_Collatz $(LDFLAGS)

# run/test files, compile with make all
FILES :=         \
    run_Collatz  \
    test_Collatz

# compile all
all: $(FILES)

# execute test harness with coverage
test: test_Collatz
	$(VALGRIND) ./test_Collatz
ifeq ($(shell uname -s), Darwin)
	$(GCOV) Collatz.cpp | grep -B 2 "cpp.gcov"
else
	$(GCOV) test_Collatz-Collatz.cpp | grep -B 2 "cpp.gcov"
endif

# clone the Collatz test repo
../cs371p-collatz-tests:
	git clone https://gitlab.com/gpdowning/cs371p-collatz-tests.git ../cs371p-collatz-tests

# test files in the Collatz test repo
T_FILES := `ls ../cs371p-collatz-tests/*.in.txt`

# generate a random input file
ctd-generate:
	for v in {1..100}; do $(CHECKTESTDATA) -g run_Collatz.ctd.txt >> run_Collatz.gen.txt; done

# execute the run harness against a test file in the Collatz test repo and diff with the expected output
../cs371p-collatz-tests/%: run_Collatz
	$(CHECKTESTDATA) run_Collatz.ctd.txt $@.in.txt
	./run_Collatz < $@.in.txt > run_Collatz.tmp.txt
	diff run_Collatz.tmp.txt $@.out.txt

# execute the run harness against your test files in the Collatz test repo and diff with the expected output
run: ../cs371p-collatz-tests
	make ../cs371p-collatz-tests/gpdowning-run_Collatz # change gpdowning to your GitLab-ID

# execute the run harness against all of the test files in the Collatz test repo and diff with the expected output
run-all: ../cs371p-collatz-tests
	-for v in $(T_FILES); do make $${v/.in.txt/}; done

# auto format the code
format:
	$(ASTYLE) Collatz.cpp
	$(ASTYLE) Collatz.hpp
	$(ASTYLE) run_Collatz.cpp
	$(ASTYLE) test_Collatz.cpp

# you must edit Doxyfile and
# set EXTRACT_ALL     to YES
# set EXTRACT_PRIVATE to YES
# set EXTRACT_STATIC  to YES
# create Doxfile
Doxyfile:
	$(DOXYGEN) -g

# create html directory
html: Doxyfile
	$(DOXYGEN) Doxyfile

# check files, check their existence with make check
C_FILES :=          \
    .gitignore      \
    .gitlab-ci.yml  \
    Collatz.log.txt \
    html

# check the existence of check files
check: $(C_FILES)

# remove executables and temporary files
clean:
	rm -f  *.gcda
	rm -f  *.gcno
	rm -f  *.gcov
	rm -f  *.gen.txt
	rm -f  *.tmp.txt
	rm -f  run_Collatz
	rm -f  test_Collatz
	rm -rf *.dSYM

# remove executables, temporary files, and generated files
scrub:
	make clean
	rm -f  Collatz.log.txt
	rm -f  Doxyfile
	rm -rf html
	rm -rf latex

# output versions of all tools
versions:
	uname -p

	@echo
	uname -s

	@echo
	which $(ASTYLE)
	@echo
	$(ASTYLE) --version

	@echo
	which $(CHECKTESTDATA)
	@echo
	$(CHECKTESTDATA) --version | head -n 1

	@echo
	which $(CPPCHECK)
	@echo
	$(CPPCHECK) --version

	@echo
	which $(DOXYGEN)
	@echo
	$(DOXYGEN) --version

	@echo
	which $(CXX)
	@echo
	$(CXX) --version | head -n 1

	@echo
	which $(GCOV)
	@echo
	$(GCOV) --version | head -n 1

	@echo
	which git
	@echo
	git --version

	@echo
	which make
	@echo
	make --version | head -n 1

ifneq ($(VALGRIND),)
	@echo
	which $(VALGRIND)
	@echo
	$(VALGRIND) --version
endif

	@echo
	which vim
	@echo
	vim --version | head -n 1

	@echo
	grep "#define BOOST_LIB_VERSION " $(BOOST)/version.hpp

	@echo
	ls -al $(GTEST)/gtest.h
	@echo
	pkg-config --modversion gtest
	@echo
	ls -al $(LIB)/libgtest*.a
