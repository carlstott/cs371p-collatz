// ----------------
// test_Collatz.cpp
// ----------------

// https://github.com/google/googletest
// https://github.com/google/googletest/blob/master/docs/reference/assertions.md

// --------
// includes
// --------

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// --------------
// CollatzFixture
// --------------

TEST(CollatzFixture, collatz_eval_0) {
	const tuple_type_1 t1 = {1, 10};
	const tuple_type_2 t2 = collatz_eval(t1);
	const tuple_type_2 t3 = {1, 10, 11};
    ASSERT_TRUE(t2 == t3);}

TEST(CollatzFixture, collatz_eval_1) {
	const tuple_type_1 t1 = {100, 200};
	const tuple_type_2 t2 = collatz_eval(t1);
	const tuple_type_2 t3 = {100, 200, 300};
    ASSERT_TRUE(t2 == t3);}

TEST(CollatzFixture, collatz_eval_2) {
	const tuple_type_1 t1 = {201, 210};
	const tuple_type_2 t2 = collatz_eval(t1);
	const tuple_type_2 t3 = {201, 210, 411};
    ASSERT_TRUE(t2 == t3);}

TEST(CollatzFixture, collatz_eval_3) {
	const tuple_type_1 t1 = {900, 1000};
	const tuple_type_2 t2 = collatz_eval(t1);
	const tuple_type_2 t3 = {900, 1000, 1900};
    ASSERT_TRUE(t2 == t3);}
