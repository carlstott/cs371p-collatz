FROM gcc

# update Linux package manager
RUN apt-get update

# auto formatter
RUN apt-get -y install astyle

# static analyzer
RUN apt-get -y install cppcheck

# configuration tool
RUN apt-get -y install cmake

# line-ending converter
RUN apt-get -y install dos2unix

# documentation generator
RUN apt-get -y install doxygen

# graph visualization, required by GMP
RUN apt-get -y install graphviz

# memory checker
RUN apt-get -y install valgrind

# editor
RUN apt-get -y install vim

# Boost library, required by checktestdata
RUN apt-get -y install libboost-dev
RUN apt-get -y install libboost-serialization-dev

# GNU bignum library, required by checktestdata
RUN apt-get -y install libgmp-dev

# Google Test
RUN apt-get -y install libgtest-dev

# build checktestdata, an input verifier
RUN git clone https://github.com/DOMjudge/checktestdata checktestdata && \
    cd checktestdata                                                  && \
    git checkout release                                              && \
    ./bootstrap                                                       && \
    make                                                              && \
    cp checktestdata /usr/bin                                         && \
    cd -

# build Google Test
RUN cd /usr/src/gtest                                                 && \
    cmake CMakeLists.txt                                              && \
    make                                                              && \
    cp lib/*.a /usr/lib                                               && \
    cd -

# Bash shell
CMD bash
