# CS371p: Object-Oriented Programming Collatz Repo

* Name: Carl Stott

* EID: cms8498

* GitLab ID: carlstott

* HackerRank ID: carlstott

* Git SHA: (most recent Git SHA, final change to your repo will change this, that's ok)

* GitLab Pipelines: (link to your GitLab CI Pipeline)

* Estimated completion time: (estimated time in hours, int or float)

* Actual completion time: (actual time in hours, int or float)

* Comments: (any additional comments you have)
