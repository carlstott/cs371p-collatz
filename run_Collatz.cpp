// ---------------
// run_Collatz.cpp
// ---------------

// --------
// includes
// --------



#include "Collatz.hpp"

// ------------
// collatz_read
// ------------

tuple_type_1 collatz_read () {
    int i, j;
    cin >> i >> j; //cin is the thing that knows to look at the cmd
    return {i, j};}

    //this asks for the user to imput 2 ints from the cmd

// -------------
// collatz_print
// -------------

void collatz_print (const tuple_type_2& t2) {
            //ok so this is sort of complex. const is used to spesify that t2 cannot be modified within
            //the collatz_print function. & indicates that t2 is a reference to an existing object instead
            //of a copy of that object. meaning that any changes made to t2 inside this function will affect
            //the origonal object [assed into it. I dont know what that last part means. Overall, the statement
            //means that we are making changes to t2 which is of object type tuple_type 2 and not changing the
            //object we are passing into the statment (in this case we are passing in the tuple_type_2 object
            //t1, calling it t2, and then doing stuff to it without messing with t1)

	auto [i, j, v] = t2;
            //auto automatically decides what variables i,j and v will be. This statement extracts the 
            //elements of t2 and binds them to i,j,and v.

    cout << i << " " << j << " " << v << endl;}
            //this prints i j v with spaces inbetween.endl is the same as a newline

// ----
// main
// ----

int main () {
    while (true) {
        tuple_type_1 t1 = collatz_read(); //this line sets t1 equil to the values that the user imputs in the cmd line
        if (!cin)
            break;
        collatz_print(collatz_eval(t1));} //we send the result of collatz_eval(t1) into the collatz_print function.
                                          //t1 had better be 3 ints at this point
    return 0;}

/*
sample input

1 10
100 200
201 210
900 1000
*/

/*
sample output

1 10 20
100 200 125
201 210 89
900 1000 174
*/
