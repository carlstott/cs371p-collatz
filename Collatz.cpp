// -----------
// Collatz.cpp
// -----------

// --------
// includes
// --------

#include <cassert> // assert

#include "Collatz.hpp"

// ------------
// collatz_eval
// ------------

tuple_type_2 collatz_eval (const tuple_type_1& t1) {
	auto [i, j] = t1;
    assert(i > 0);
    assert(j > 0);
    int n;
    
    for (int x=i; x <=j; x++){
        int ii=i;
        //I will be messing with ii, but will not touch i so my for loop does not break.
        n=0;
        
        while (ii >1){
            //we are not reaching here when ii=1, good
            //cout <<69;


            if (ii%2 == 0){ //meaning that there is no remainder, meaning that i is even
            ii=ii/2;
            n=n+1;

            } else { //if i&2 /=0, then i must be odd
            ii=(3*ii + 1)/2; // we increment the algorithem twice in 1 step, so we add 2 to n
            n=n+2;
            
            }
        }
    }
    //cout <<'T' <<i << j << n << 'T';

    return tuple_type_2(i, j, n);
}
